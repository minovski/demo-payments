<!DOCTYPE HTML>
<html>
<head>
    <link rel="stylesheet" href="includes/bootstrap.min.css">
    <script src="includes/jquery-3.3.1.min.js"></script>
    <style>
        button {
            margin-top: 5px;
        }
    </style>
</head>
<body style="background-color: green">

<div class="container" style="margin: 100px; background-color: white; padding: 30px">
    <form method="POST" action="">
        <div id="first_tab" class="form-group">
            <h4 class="text-center">Personal Information</h4>
            <label for="firstName">First Name</label>
            <input type="text" class="form-control" name="first" id="first" value=""
                   placeholder="Please insert first name" required>
            <label for="lastName">Last Name</label>
            <input type="text" class="form-control" name="last" id="last" value="" placeholder="Please insert last name"
                   required>
            <label for="tel">Telephone</label>
            <input type="text" class="form-control" name="tel" id="tel" value="" placeholder="Please insert telephone"
                   required>
            <button id="next_second" class="btn btn-primary float-right">NEXT</button>
        </div>
        <div id="second_tab" class="form-group">
            <h4 class="text-center">Address information</h4>
            <label for="street">Street</label>
            <input type="text" class="form-control" name="street" id="street" value=""
                   placeholder="Please insert street" required>
            <label for="house_number">House No.</label>
            <input type="text" class="form-control" name="house" id="house" value=""
                   placeholder="Please insert house number" required>
            <label for="zip">Zip Code</label>
            <input type="text" class="form-control" name="zip" id="zip" value="" placeholder="Please insert zip code"
                   required>
            <label for="city">City</label>
            <input type="text" class="form-control" name="city" id="city" value="" placeholder="Please insert city"
                   required>
            <button id="next_third" class="btn btn-primary float-right">NEXT</button>
            <button id="prev_first" class="btn btn-danger">PREV</button>
        </div>
        <div id="third_tab" class="form-group">
            <h4 class="text-center">Payment Information</h4>
            <label for="owner">Account Owner</label>
            <input type="text" class="form-control" name="owner" id="owner" value=""
                   placeholder="Please insert account owner" required>
            <label for="iban">IBAN</label>
            <input type="text" class="form-control" name="iban" id="iban" value="" placeholder="Please insert IBAN"
                   required>
            <button id="submit" class="btn btn-primary float-right">SUBMIT</button>
            <button id="prev_second" class="btn btn-danger">PREV</button>
        </div>
        <div id="fourth_tab" class="form-group">
            <h2 class="text-center"></h2>
            <div class="font-weight-bold text-center"></div>
        </div>

    </form>
</div>


<?php if (isset($_COOKIE['onSecondPage']) && $_COOKIE['onSecondPage'] == true) { ?>

    <script>
        $('#first_tab').hide();
        $('#second_tab').show();
        $('#third_tab').hide();
        $('#fourth_tab').hide();
    </script>

<?php } elseif (isset($_COOKIE['onThirdPage']) && $_COOKIE['onThirdPage'] == true){ ?>

    <script>
        $('#first_tab').hide();
        $('#second_tab').hide();
        $('#third_tab').show();
        $('#fourth_tab').hide();
    </script>

<?php } else { ?>

    <script>
        $('#second_tab').hide();
        $('#third_tab').hide();
        $('#fourth_tab').hide();
    </script>

<?php } ?>

<script>

    $('#next_second').on('click', function (e) {

        e.preventDefault();

        if ($('#first').val() == '' || $('#last').val() == '' || $('#tel').val() == '') {
            alert("Please fill all fields");
        } else {
            $('#first_tab').hide();
            $('#second_tab').show();
            $('#third_tab').hide();
            $('#fourth_tab').hide();
            var first = $('#first').val();
            var last = $('#last').val();
            var tel = $('#tel').val();

            $.ajax({
                url: 'store.php',
                type: 'POST',
                data: {
                    first: first,
                    last: last,
                    tel: tel
                }
            });
        }
    });

    $('#prev_first').on('click', function (e) {
        e.preventDefault();
        $('#first_tab').show();
        $('#second_tab').hide();
        $('#third_tab').hide();
        $('#fourth_tab').hide();

    });
    $('#next_third').on('click', function (e) {
        e.preventDefault();
        if ($('#street').val() == '' || $('#house').val() == '' || $('#zip').val() == '' || $('#city').val() == '') {
            alert("Please fill all fields");
        } else {
            $('#first_tab').hide();
            $('#second_tab').hide();
            $('#third_tab').show();
            $('#fourth_tab').hide();

            var street = $('#street').val();
            var house = $('#house').val();
            var zip = $('#zip').val();
            var city = $('#city').val();

            $.ajax({
                url: 'store.php',
                type: 'POST',
                data: {
                    street: street,
                    house: house,
                    zip: zip,
                    city: city
                }
            });
        }
    });
    $('#prev_second').on('click', function (e) {
        e.preventDefault();
        $('#first_tab').hide();
        $('#second_tab').show();
        $('#third_tab').hide();
        $('#fourth_tab').hide();

    });

    $('#submit').on('click', function (e) {
        e.preventDefault();
        if ($('#owner').val() == '' || $('#iban').val() == '') {
            alert("Please fill all fields");
        } else {
            $('#first_tab').hide();
            $('#second_tab').hide();
            $('#third_tab').hide();

            var owner = $('#owner').val();
            var iban = $('#iban').val();

            var a = '';
            $.ajax({
                url: 'store.php',
                type: 'POST',
                data: {
                    submit: true,
                    owner: owner,
                    iban: iban

                }, success: function (result) {

                    var json = JSON.parse(result);
                    var id = json.customerId;
                    var owner = json.owner;
                    var iban = json.iban;

                    var url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: JSON.stringify({
                            customerId: id,
                            iban: iban,
                            owner: owner
                        }),
                        dataType: 'json',
                        crossDomain: true,
                        success: function (response) {

                            var payId = response.paymentDataId;

                            $.ajax({
                                url: 'store.php',
                                data: {dataPaymentId: payId},
                                method: "POST",
                                success: function (response) {
                                    $('#fourth_tab').show();
                                    $('#fourth_tab').find('h2').append('SUCCESS');
                                    $('#fourth_tab').find('div').append('Data payment id is ' + response);

                                }
                            });

                        },
                        error: function (error,jqXHR, textStatus) {
                            $('#fourth_tab').show();
                            $('#fourth_tab').find('h2').append('FAILED');
                            $('#fourth_tab').find('div').append('An error occurred' + jqXHR + textStatus);
                        }
                    });

                }
            });
        }
    });


</script>

</body>
</html>