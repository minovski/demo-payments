<?php
include 'includes/db.php';

if (isset($_POST['first']) && isset($_POST['last']) && isset($_POST['tel'])) {

    $first_s = $_POST['first'];
    $last_s = $_POST['last'];
    $tel_s = $_POST['tel'];
    unset($_COOKIE['onThirdPage']);
    setcookie("onThirdPage", '', time() - 3600);
    setcookie("onSecondPage", true, time() + (60 * 2)); // expires after 2 min
    $sql = "INSERT INTO personal (user_first, user_last, user_tel) VALUES('$first_s', '$last_s', '$tel_s')";
    $query = mysqli_query($conn, $sql);

}


if (isset($_POST['street']) && isset($_POST['house']) && isset($_POST['zip']) && isset($_POST['city'])) {

    $street_s = $_POST['street'];
    $house_s = $_POST['house'];
    $zip_s = $_POST['zip'];
    $city_s = $_POST['city'];
    unset($_COOKIE['onSecondPage']);
    setcookie('onSecondPage', '', time() - 3600);
    setcookie("onThirdPage", true, time() + (60 * 2)); // expire after 2 min

    $sql = "INSERT INTO address (street, house_no, zip_code, city, customerId) VALUES('$street_s', '$house_s', '$zip_s', '$city_s', (SELECT MAX(customerId) FROM personal))";
    $query = mysqli_query($conn, $sql);
}


if (isset($_POST['submit']) && $_POST['submit'] == true) {

    $owner_s = $_POST['owner'];
    $iban_s = $_POST['iban'];

    $sql = "INSERT INTO payment (owner, iban, customerId) VALUES('$owner_s', '$iban_s', (SELECT MAX(customerId) FROM personal))";
    $query = mysqli_query($conn, $sql);

    unset($_COOKIE['onSecondPage']);
    setcookie('onSecondPage', '', time() - 3600);
    unset($_COOKIE['onThirdPage']);
    setcookie('onThirdPage', '', time() - 3600);

    $res = "SELECT customerId, owner, iban FROM payment WHERE customerId=(SELECT MAX(customerId) FROM personal)";
    $res_query = mysqli_query($conn, $res);

    while ($row = mysqli_fetch_array($res_query)) {
        $customer_id = $row['customerId'];
        $iban = $row['iban'];
        $owner = $row['owner'];

        $data = ["customerId" => $customer_id, "iban" => $iban, "owner" => $owner];

        die(json_encode($data));
    }

}


if (isset($_POST['dataPaymentId'])) {

    $payment_id = $_POST['dataPaymentId'];

    $sql_payment = "UPDATE payment SET data_payment_id= '" . $payment_id . "' WHERE customerId=(SELECT MAX(customerId) FROM personal)";
    $result_payment = mysqli_query($conn, $sql_payment);

    echo $payment_id;
}